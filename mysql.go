package adapter

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"sync"
	"time"
)

type Mysqls map[string]*Mysql

// Get Mysql
func (adapters Mysqls) Get(name string) (result *Mysql) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Could not found Mysql config " + name)
	}
	return
}

type Mysql struct {
	Name			string 	`mapstructure:"name"`
	Address 		string	`mapstructure:"address"`
	Port 			int		`mapstructure:"port"`
	Username 		string	`mapstructure:"username"`
	Password 		string	`mapstructure:"password"`
	DatabaseName 	string 	`mapstructure:"database_name"`
	MaxIdleConn 	int		`mapstructure:"max_idle_conn"`
	MaxOpenConn 	int		`mapstructure:"max_open_conn"`
	SingularTable	bool 	`mapstructure:"singular_table"`
	Session 		*gorm.DB
}

var (
	onceMysql map[string]*sync.Once
	onceMysqlMutex = sync.RWMutex{}
)

func init() {
	onceMysql = make(map[string]*sync.Once)
}

func (config *Mysql) Init() {
	if onceMysql[config.Name] == nil {
		onceMysql[config.Name] = &sync.Once{}
	}
	onceMysql[config.Name].Do(func() {
		onceMysqlMutex.Lock()
		log.Printf("[%s][%s] Mysql [connecting]\n", config.Name, config.Address)

		connString := fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", config.Username, config.Password, config.DatabaseName)
		db, err := gorm.Open("mysql", connString)
		if err != nil {
			log.Printf("[%s][%s] Mysql [error]: %s", config.Name, config.Address, err)
			time.Sleep(1 * time.Second)
			onceMysql[config.Name] = &sync.Once{}
			onceMysqlMutex.Unlock()
			config.Init()
		} else {
			config.Session = db
			config.Session.SingularTable(config.SingularTable)
			config.Session.DB().SetMaxOpenConns(config.MaxOpenConn)
			config.Session.DB().SetMaxIdleConns(config.MaxIdleConn)

			// try connect
			errPing := config.Session.DB().Ping()
			if errPing != nil {
				log.Printf("[%s][%s] Mysql [ping error]: %s", config.Name, config.Address, errPing)
				time.Sleep(1 * time.Second)
				onceMysql[config.Name] = &sync.Once{}
				onceMysqlMutex.Unlock()
				config.Init()
				return
			}
			log.Printf("[%s][%s] Mysql [connected]", config.Name, config.Address)
			onceMysqlMutex.Unlock()
		}
	})
}

func (config Mysql) GetMysqlSession() (session *gorm.DB) {
	if config.Session != nil {
		session = config.Session
	} else {
		panic(fmt.Errorf("[%s][%s] Have not init Mysql", config.Name, config.Address))
	}
	return
}